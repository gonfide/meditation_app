class Preset {

  int Id;
  String Name;
  String Description;
  int Preparation;
  int Duration;
  bool BellRing;
  int BellRingInterval;
  bool EndIndicator;
  bool Music;
  String MusicName;
  String BillSound;
  String EndMusic;
  String DurationStr;
  String Date;
Preset({
  this.Id,
  this.Name,
  this.Description,
  this.Preparation,
  this.Duration,
  this.BellRing,
  this.BellRingInterval,
  this.BillSound,
  this.EndMusic,
  this.EndIndicator,
  this.Music,
  this.MusicName,
  this.DurationStr,
  this.Date,

});
  Map<String, dynamic> toMap() {
    return {
    'Id':Id,
    'Name':Name,
    'Description':Description,
    'Preparation':Preparation,
    'Duration':Duration,
    'BellRing':BellRing,
      'BillSound':BillSound,
     'EndMusic':EndMusic,
    'BellRingInterval': BellRingInterval,
    'EndIndicator':EndIndicator,
    'Music':Music,
    'MusicName':MusicName,
    'Date':Date,
    };
  }
  Preset.fromMapObject(Map<String, dynamic> map) {
    this.Id=map['Id'];
    this.Name=map['Name'];
    this.Description=map['Description'];
    this.Preparation=map['Preparation'];
    this.Duration=map['Duration'];
    this.BellRing=true;  //map['BellRing'];
    this.BellRingInterval=map['BellRingInterval'];
    this.EndIndicator=true;//map['EndIndicator'];
    this.Music= true; //map['Music'];
    this.MusicName=map['MusicName'];
    this.MusicName=map['BillSound'];
    this.MusicName=map['EndMusic'];
    this.Date=map['Date'];

  }


}