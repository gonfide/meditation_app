import 'package:boilerplate/models/meditation/preset.dart';

class Session extends Preset {
  int Id;
  String Name;
  String Description;
  int Preparation;
  int Duration;
  bool BellRing;
  int BellRingInterval;
  bool EndIndicator;
  bool Music;
  String MusicName;
  String BillSound;
  String EndMusic;
  String DurationStr;
  String Date;
  bool isDone;

  Session(Preset pr,bool isDone){
    this.Name=this.Name;
    this.Description=pr.Description;
    this.Preparation=pr.Preparation;
    this.Duration=pr.Duration;
    this.BellRing=pr.BellRing;
    this.BellRingInterval=pr.BellRingInterval;
    this.BillSound=pr.BillSound;
    this.EndMusic=pr.EndMusic;
    this.EndIndicator=pr.EndIndicator;
    this.Music=pr.Music;
    this.MusicName=pr.MusicName;
    this.DurationStr=pr.DurationStr;
    this.Date=pr.Date;
  }

  Map<String, dynamic> toMap() {
    return {
    'Id':Id,
    'Name':Name,
    'Description':Description,
    'Preparation':Preparation,
    'Duration':Duration,
    'BellRing':BellRing,
      'BillSound':BillSound,
     'EndMusic':EndMusic,
    'BellRingInterval': BellRingInterval,
    'EndIndicator':EndIndicator,
    'Music':Music,
    'MusicName':MusicName,
    'Date':Date,
    };
  }
  Session.fromMapObject(Map<String, dynamic> map) {
    this.Id=map['Id'];
    this.Name=map['Name'];
    this.Description=map['Description'];
    this.Preparation=map['Preparation'];
    this.Duration=map['Duration'];
    this.BellRing=true;  //map['BellRing'];
    this.BellRingInterval=map['BellRingInterval'];
    this.EndIndicator=true;//map['EndIndicator'];
    this.Music= true; //map['Music'];
    this.MusicName=map['MusicName'];
    this.MusicName=map['BillSound'];
    this.MusicName=map['EndMusic'];
    this.Date=map['Date'];

  }


}