class User {
   int id;
   String name;
   int hours;
   int sessions;
  User({this.id, this.name, this.hours,this.sessions});

  Map<String, dynamic> toMap() {
    return {
      'Id': id,
      'Name': name,
      'Hours': hours,
      'Sessions':sessions,
    };
  }

   User.fromMapObject(Map<String, dynamic> map) {
     this.id=map['Id'];
     this.name=map['Name'];
     this.hours=map['Hours'];
     this.sessions=map['Sessions'];

   }

}
