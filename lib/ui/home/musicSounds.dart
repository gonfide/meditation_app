
import 'package:boilerplate/utils/musicPlayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../routes.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import '../../models/meditation/preset.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';

class MusicSoundsScreen extends StatefulWidget {

  @override
  _MusicSoundsScreenState createState() => _MusicSoundsScreenState();
}

class _MusicSoundsScreenState extends State<MusicSoundsScreen> {

  @override
  Widget build(BuildContext context) {
    Preset preset=ModalRoute.of(context).settings.arguments[0];
    String param =ModalRoute.of(context).settings.arguments[1];
    return Scaffold(
      body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,

                children: <Widget>[
                  new InkWell(// this is the one you are looking for..........
                    onTap: () =>  {
                      preset.MusicName="music 1",
                      Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    },
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("SOUND 1", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  ),
                  new InkWell(// this is the one you are looking for..........
                    onTap: () =>  {
                      preset.MusicName="music 2",
                      Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    },
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("SOUND 2", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  ),
                  new InkWell(// this is the one you are looking for..........
                    onTap: () =>  {
                      //mp.playSound(),
                      // play(),
                      // this.imp.playAudio(),
                      //_playSound,
                      Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    },
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("SOUND 3", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  ),
                ],
              ),
              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,

                children: <Widget>[
                  new InkWell(// this is the one you are looking for..........
                    onTap: () =>  {
                      // this.imp.stop(),
                      Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    },
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("SOUND 4", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  ),
                  new InkWell(// this is the one you are looking for..........
                    onTap: () =>  {
                      // this.imp.stop(),
                      Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    },
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("SOUND 5", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  )
                ],
              ),
              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,

                children: <Widget>[
                  new InkWell(// this is the one you are looking for..........
                    onTap: () => Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                    child: new Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,// You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.orangeAccent,
                      ),
                      child: new Text("Music x", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                    ),//............
                  )
                ],
              )
            ],
          )),
    );
  }



}
