import 'package:boilerplate/models/meditation/preset.dart';
import 'package:boilerplate/models/meditation/session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../routes.dart';
import '../../models/meditation/preset.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'dart:io';

//static Preset preset;
class MyPresetsScreen extends StatefulWidget {
  @override
  _MyPresetsScreenState createState() => _MyPresetsScreenState();
}

class _MyPresetsScreenState extends State<MyPresetsScreen> {
  final List<int> colorCodes = <int>[600, 500, 100];
  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Preset> presetList;
  Preset preset;
  int count = 0;

  @override
  void initState() {
    super.initState();
    updateListView();
   // updateListView();
  }





  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //length: 1,
      onWillPop: () {
            moveToLastScreen();
      },
      child: Scaffold(
        appBar: AppBar(
          //title: const Text('My sessions'),
          leading: IconButton(
              icon:Icon(Icons.arrow_back),
              onPressed: () => moveToLastScreen()
            ),
            ),


        body:
        (this.count) == 0 ?
         buildEmpty() :   buildTabFinal() ,

      ),
    );
  }

  Widget buildTabFinal() {
    updateListView();

    return ListView.separated(

      itemCount: this.count,
      itemBuilder: (BuildContext context, int index) {


        return Material(

          child: InkWell(
            onTap: () { Navigator.of(context).pushReplacementNamed(Routes.timer,arguments:[this.presetList[index]]);},
            child: ListTile(
              title: Center(child: Text('Preset : ${this.presetList[index].Name+" " + (index+1).toString() }')),
              subtitle: Text('duration: ${this.presetList[index].DurationStr}'),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      //  anchor: 0.5,
    );
  }


  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () =>Navigator.of(context).pop(true),
                //Navigator.pop(context),

            child: new Text('Yes'),
          ),
        ],
      ),
    )) ?? false;
  }

  void updateListView() {

    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {

      Future<List<Preset>> presetListFuture = databaseHelper.getPresetList();
      presetListFuture.then((presetList) {
        setState(() {
          this.presetList = presetList;
          for (int i = 0; i < this.count; i++) {
            this.presetList[i].DurationStr=updateDurationStr(this.presetList[i].Duration);
          }
          this.count = this.presetList.length;
        });
      });
    });
  }

  void moveToLastScreen() {
    Navigator.of(context).pushReplacementNamed(Routes.menu);
  //  Navigator.pop(context, true);
  }

  // Save data to database
  void _save() async {

    moveToLastScreen();

    preset.Date = DateFormat.yMMMd().format(DateTime.now());
    int result;
    if (preset.Id != null) {  // Case 1: Update operation
      result = await databaseHelper.updatePreset(preset);
    } else { // Case 2: Insert Operation
      result = await databaseHelper.insertPreset(preset);
    }

    if (result != 0) {  // Success
      _showAlertDialog('Status', 'Note Saved Successfully');
    } else {  // Failure
      _showAlertDialog('Status', 'Problem Saving Note');
    }

  }

  void _delete() async {

    moveToLastScreen();

    // Case 1: If user is trying to delete the NEW NOTE i.e. he has come to
    // the detail page by pressing the FAB of NoteList page.
    if (preset.Id == null) {
      _showAlertDialog('Status', 'No Note was deleted');
      return;
    }

    // Case 2: User is trying to delete the old note that already has a valid ID.
    int result = await databaseHelper.deletePreset(preset.Id);
    if (result != 0) {
      _showAlertDialog('Status', 'Note Deleted Successfully');
    } else {
      _showAlertDialog('Status', 'Error Occured while Deleting Note');
    }
  }

  void _showAlertDialog(String title, String message) {

    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(
        context: context,
        builder: (_) => alertDialog
    );
  }

  String updateDurationStr(int duration) {
    int dur=duration*60;
    var xx=((((dur / 60)/60)) % 60);
   // dur=int.parse(xx.);
    String Hs =((((dur / 60)/60)) % 60).toInt().toString();
    Hs=int.parse(Hs).toString();
    String ms =((dur / 60)% 60).toInt().toString();
    ms=int.parse(ms).toString();
    String ss =(dur % 60).toInt().toString();
    ss=int.parse(ss).toString();
    //if(Hs.length<1) ss="0"+ss;
    String durStr =ss+" : "+ms+" : "+ss;
    return durStr;
    //preset.DurationStr = ss+" : "+ms+" : "+ss;
  }

  Widget buildEmpty() {

    return Center(child: Text("You don't have any sessions yet !"));


  }

}
