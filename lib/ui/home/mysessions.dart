import 'package:boilerplate/models/meditation/preset.dart';
import 'package:boilerplate/models/meditation/session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../routes.dart';
import '../../models/meditation/preset.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'dart:io';

//static Preset preset;
class MySessionsScreen extends StatefulWidget {
  @override
  _MySessionsScreenState createState() => _MySessionsScreenState();
}

class _MySessionsScreenState extends State<MySessionsScreen> {
  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Session> sessionList;
  Session session;
  int count = 0;
  @override
  void initState() {
    super.initState();
   // updateListView();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //length: 1,
      onWillPop: () {
            moveToLastScreen();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          //title: const Text('My sessions'),
          leading: IconButton(
              icon:Icon(Icons.arrow_back),
              onPressed: () => moveToLastScreen()
            ),
            ),


        body: buildTabFinal(),

      ),
    );
  }

  Widget buildTabFinal() {
    updateListViewFinal();
    return ListView.separated(

      itemCount: this.count,
      itemBuilder: (BuildContext context, int index) {

        return Material(
          child: InkWell(
            onTap: () { },
            child: ListTile(
              title: Center(child: Text('Session: ${this.sessionList[index].Id.toString()+" "}')),
              subtitle: Text('duration: ${this.sessionList[index].DurationStr}'),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      //  anchor: 0.5,
    );
  }


  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () =>Navigator.of(context).pop(true),
                //Navigator.pop(context),

            child: new Text('Yes'),
          ),
        ],
      ),
    )) ?? false;
  }

  void updateListViewFinal() {

    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Session>> sessionListFuture = databaseHelper.getSessionList();
      setState(() {
        sessionListFuture.then((sessionList) {
          this.sessionList=sessionList;
          this.count=sessionList.length;
          for (int i = 0; i < sessionList.length; i++) {

            this.sessionList[i].DurationStr = updateDurationStr(this.sessionList[i].Duration);

          }
      });
      });
    });
  }

  void moveToLastScreen() {
    Navigator.of(context).pushReplacementNamed(Routes.menu);
  //  Navigator.pop(context, true);
  }

  // Save data to database




  void _showAlertDialog(String title, String message) {

    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(
        context: context,
        builder: (_) => alertDialog
    );
  }

  String updateDurationStr(int duration) {
    int dur=duration*60;
    var xx=((((dur / 60)/60)) % 60);
   // dur=int.parse(xx.);
    String Hs =((((dur / 60)/60)) % 60).toInt().toString();
    Hs=int.parse(Hs).toString();
    String ms =((dur / 60)% 60).toInt().toString();
    ms=int.parse(ms).toString();
    String ss =(dur % 60).toInt().toString();
    ss=int.parse(ss).toString();
    //if(Hs.length<1) ss="0"+ss;
    String durStr =ss+" : "+ms+" : "+ss;
    return durStr;
    //preset.DurationStr = ss+" : "+ms+" : "+ss;
  }

}
