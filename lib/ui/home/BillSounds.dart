
import 'package:boilerplate/utils/musicPlayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../routes.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import '../../models/meditation/preset.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';

class BillSoundScreen extends StatefulWidget {

  @override
  _BillSoundScreenState createState() => _BillSoundScreenState();
}

class _BillSoundScreenState extends State<BillSoundScreen> {

  @override
  Widget build(BuildContext context) {
    Preset preset=ModalRoute.of(context).settings.arguments[0];
    String param =ModalRoute.of(context).settings.arguments[1];
    return Scaffold(
      body: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          SizedBox(height: 40),
          new Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,

            children: <Widget>[
               new InkWell(// this is the one you are looking for..........
                 onTap: () =>  {
                   if(param=="billSound") {preset.BillSound="bill sound 1"}
                   else {preset.EndMusic="bill sound 1"},

                   Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                               },
                child: new Container(

                  padding: const EdgeInsets.all(40.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.amberAccent,
                  ),
                  child: new Text("BILL SOUND 1", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
                 ),
              new InkWell(// this is the one you are looking for..........
                onTap: () =>  {
                  if(param=="billSound") {preset.BillSound="bill Sound 2"}
                  else {preset.EndMusic="bill Sound 2"},
                  Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                },
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(40.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.amberAccent,
                  ),
                  child: new Text("BILL SOUND 2", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              ),

            ],
          ),
          SizedBox(height: 50),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              new InkWell(// this is the one you are looking for..........
                  onTap: () =>  {
                    if(param=="billSound") {preset.BillSound="bill Sound 3"}
                    else {preset.EndMusic="bill Sound 3"},
                    Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
      },
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(40.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.amberAccent,
                  ),
                  child: new Text("BILL SOUND 3", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              ),
              new InkWell(// this is the one you are looking for..........
                onTap: () =>  {
                  if(param=="billSound") {preset.BillSound="bill Sound 4"}
                  else {preset.EndMusic="bill Sound 4"},
                  Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),
                },
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(40.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.amberAccent,
                  ),
                  child: new Text("BILL SOUND 4", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              )
            ],
          ),
          SizedBox(height: 50),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

            children: <Widget>[
              new InkWell(// this is the one you are looking for..........
                onTap: () => {
                  if(param=="billSound") {preset.BillSound="bill Sound X"}
                  else {preset.EndMusic="bill Sound X"},
                  Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]),},
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(40.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.amberAccent,
                  ),
                  child: new Text("BILL SOUND X", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              )
            ],
          )
        ],
      )),
    );
  }



}
