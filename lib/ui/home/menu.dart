
import 'package:boilerplate/utils/musicPlayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../routes.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import '../../models/meditation/preset.dart';
import '../../models/meditation/session.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';

class MenuScreen extends StatefulWidget {

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {


  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Preset> presetList;
  @override
  Widget build(BuildContext context) {
    getLastPreset();
    return Scaffold(
      body: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 10),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: <Widget>[
            new IconButton(
                    icon: new Icon(Icons.ac_unit),
                    onPressed:  () =>  {
                      Navigator.of(context).pushReplacementNamed(Routes.profile)
                    }
                  ),
              IconButton(
                    icon: new Icon(Icons.settings),
                  onPressed:  () =>  {
                    Navigator.of(context).pushReplacementNamed(Routes.settings)
                  }
                  ),
            ],
          ),
          SizedBox(height: 40),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
               new InkWell(// this is the one you are looking for..........
                 onTap: () =>  {
                   //mp.playSound(),
                  // play(),
                  // this.imp.playAudio(),
                   //_playSound,
                   Navigator.of(context).pushReplacementNamed(Routes.mysessions),
                               },
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(60.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.green,
                  ),
                  child: new Text("LOAD SESSION", style: new TextStyle(color: Colors.white, fontSize: 20.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
                 )
            ],
          ),
          SizedBox(height: 50),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              new InkWell(// this is the one you are looking for..........
                  onTap: () =>  {
                   // this.imp.stop(),
                    Navigator.of(context).pushReplacementNamed(Routes.session),
      },
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(60.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.green,
                  ),
                  child: new Text("NEW SESSION", style: new TextStyle(color: Colors.white, fontSize: 20.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              )
            ],
          ),
          SizedBox(height: 50),
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,

            children: <Widget>[
              new InkWell(// this is the one you are looking for..........
                onTap: () => Navigator.of(context).pushReplacementNamed(Routes.timer,arguments: [this.presetList[0]]),
                child: new Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(60.0),//I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,// You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.green,
                  ),
                  child: new Text("REPEAT SESSION", style: new TextStyle(color: Colors.white, fontSize: 18.0)),// You can add a Icon instead of text also, like below.
                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                ),//............
              )
            ],
          )
        ],
      )),
    );
  }

  void getLastPreset() {

    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {

      Future<List<Preset>> presetListFuture = databaseHelper.getPresetList();
      presetListFuture.then((presetList) {
        setState(() {
          this.presetList = presetList;
          for (int i = 0; i < this.presetList.length; i++) {
         //   this.presetList[i].DurationStr=updateDurationStr(this.presetList[i].Duration);
          }
        });
      });
    });
  }

  String updateDurationStr(int duration) {
    int dur=duration*60;
    var xx=((((dur / 60)/60)) % 60);
    // dur=int.parse(xx.);
    String Hs =((((dur / 60)/60) % 60) % 60).toString();
    String ms =((dur / 60)% 60).toString();
    String ss =(dur % 60).toString();
    String durStr =ss+" : "+ms+" : "+ss;
    return durStr;
    //preset.DurationStr = ss+" : "+ms+" : "+ss;
  }

}
