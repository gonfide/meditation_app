import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:boilerplate/models/meditation/preset.dart';
import '../../routes.dart';
import '../../models/meditation/preset.dart';
import '../../sqLite/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
//static Preset preset;
class SessionScreen extends StatefulWidget {
  @override
  _SessionScreenState createState() => _SessionScreenState();
}

class _SessionScreenState extends State<SessionScreen> {

  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Preset> presetList;
  Preset preset=new Preset();
  bool buttonState = true;
  String parametre;
  bool enable1 = true;
  bool enable2 = true;
  bool enable3 = true;
  bool enable4 = true;
  bool enable5 = true;
  bool enable6 = true;

  @override
  Widget build(BuildContext context) {
    updateName();
    updatebillRingInterval(10);
    updateBellRing(true);
    updateEndIndicator();
  //  updateMusicName("my session");
   // updateDurationStr();
    if(ModalRoute.of(context).settings.arguments!=null)
    {
      preset=ModalRoute.of(context).settings.arguments[0];
      parametre=ModalRoute.of(context).settings.arguments[1];
      if(parametre=="preparation")enable1=ModalRoute.of(context).settings.arguments[2];
      else if (parametre=="duration") {
       if(preset.Preparation!=null) enable1=ModalRoute.of(context).settings.arguments[2];
        enable2 =ModalRoute.of(context).settings.arguments[2];
      }

      else if (parametre=="interval") {
        if(preset.Preparation!=null) enable1=ModalRoute.of(context).settings.arguments[2];
        if(preset.Duration!=null) enable2 =ModalRoute.of(context).settings.arguments[2];
        enable3 =ModalRoute.of(context).settings.arguments[2];
      }
    else if (parametre=="billSound") {
      if(preset.Preparation!=null) enable1=ModalRoute.of(context).settings.arguments[2];
      if(preset.Duration!=null) enable2 =ModalRoute.of(context).settings.arguments[2];
      if(preset.BellRingInterval!=null) enable3 =ModalRoute.of(context).settings.arguments[2];
      enable4=ModalRoute.of(context).settings.arguments[2];
    }
    else if (parametre=="musicSound") {
      if(preset.Preparation!=null) enable1=ModalRoute.of(context).settings.arguments[2];
      if(preset.Duration!=null) enable2 =ModalRoute.of(context).settings.arguments[2];
      if(preset.BellRingInterval!=null) enable3 =ModalRoute.of(context).settings.arguments[2];
      if(preset.BillSound!=null) enable4 =ModalRoute.of(context).settings.arguments[2];
      enable5=ModalRoute.of(context).settings.arguments[2];
    }
    else if (parametre=="endMusic") {
      if(preset.Preparation!=null) enable1=ModalRoute.of(context).settings.arguments[2];
      if(preset.Duration!=null) enable2 =ModalRoute.of(context).settings.arguments[2];
      if(preset.BellRingInterval!=null) enable3 =ModalRoute.of(context).settings.arguments[2];
      if(preset.BillSound!=null) enable4 =ModalRoute.of(context).settings.arguments[2];
      if(preset.MusicName!=null) enable5=ModalRoute.of(context).settings.arguments[2];
      enable6 =ModalRoute.of(context).settings.arguments[2];
    }
    }
    return WillPopScope(
        onWillPop: () {
      moveToLastScreen(context);
    },
    child: Scaffold(
      body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 20),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: <Widget>[
                  new IconButton(

                    icon: new Icon(Icons.arrow_back),
                    onPressed: () => Navigator.of(context).pushReplacementNamed(Routes.menu),
                  ),

                ],
              ),
              SizedBox(height: 20),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,

                children: <Widget>[
                  Column(
                    children: <Widget>[
                      enable1
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("OFF \nPreparation time"),
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"preparation"]);

                          setState(() {
                            enable1 = !enable1;});
                        },
                      )
                          : Container(),
                      !(enable1)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child: Center(child:Text("choosen Preparation\n"+preset.Preparation.toString()+" minutes"),),
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"preparation"]);
                            setState(() {
                              enable1 = !enable1;});
                          })
                          : Container()
                    ],),
                  Column(
                    children: <Widget>[
                      enable2
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("1 hour \nMeditation time"),
                        onPressed: () {

                          Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"duration"]);

                          setState(() {
                            enable2 = !enable2;});
                        },
                      )
                          : Container(),
                      !(enable2)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child: Center(child:Text("choosen Duration\n"+preset.Duration.toString()+" minutes"),),
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"duration"]);
                            setState(() {
                              enable2 = !enable2;});
                          })
                          : Container()
                    ],),
                ],
              ),
              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,

                children: <Widget>[
                  Column(
                    children: <Widget>[
                      enable3
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("OFF \nBell ring interval"),
                        onPressed: () {

                          Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"interval"]);

                          setState(() {
                            enable3 = !enable3;});
                        },
                      )
                          : Container(),
                      !(enable3)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child:  Center(child:Text("choosen interval\n"+preset.BellRingInterval.toString()+" minutes"),),
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.durationpicker,arguments:[preset,"interval"]);
                            setState(() {
                              enable3 = !enable3;});
                          })
                          : Container()
                    ],),
                  Column(
                    children: <Widget>[
                      enable4
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("OFF \nBell ring interval"),
                        onPressed: () {

                          Navigator.of(context).pushReplacementNamed(Routes.billSounds,arguments:[preset,"billSound"]);

                          setState(() {
                            enable4 = !enable4;});
                        },
                      )
                          : Container(),
                      !(enable4)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child:  Center(child:Text("Bill ring sound\n"" choosen"),), //+preset.BillSound+
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.billSounds,arguments:[preset,"billSound"]);
                            setState(() {
                              enable4 = !enable4;});
                          })
                          : Container()
                    ],),
                ],
              ),
              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,

                children: <Widget>[
                  Column(
                    children: <Widget>[
                      enable5
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("OFF\nBackgroud music"),
                        onPressed: () {

                          Navigator.of(context).pushReplacementNamed(Routes.musicSounds,arguments:[preset,"musicSound"]);

                          setState(() {
                            enable5 = !enable5;});
                        },
                      )
                          : Container(),
                      !(enable5)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child:  Center(child:Text("Backgroud music\n"" choosen"),), //+preset.MusicName+
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.musicSounds,arguments:[preset,"musicSound"]);
                            setState(() {
                              enable5 = !enable5;});
                          })
                          : Container()
                    ],),
                  Column(
                    children: <Widget>[
                      enable6
                          ? RaisedButton(
                        shape: StadiumBorder(),
                        color: Colors.blue,
                        child: Text("OFF \nSession end sound"),
                        onPressed: () {

                          Navigator.of(context).pushReplacementNamed(Routes.billSounds,arguments:[preset,"endMusic"]);

                          setState(() {
                            enable6 = !enable6;});
                        },
                      )
                          : Container(),
                      !(enable6)
                          ? RaisedButton(
                          shape: StadiumBorder(),
                          color: Colors.amber,
                          child:  Center(child:Text("Session end sound\n"" choosen"),), //+preset.EndMusic+
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed(Routes.billSounds,arguments:[preset,"endMusic"]);
                            setState(() {
                              enable6 = !enable6;});
                          })
                          : Container()
                    ],),
                ],
              ),
              SizedBox(height: 50),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,

                children: <Widget>[
                  RaisedButton(
                    shape: StadiumBorder(),
                    color: Colors.blue,
                    onPressed: () {
                     // preset=ModalRoute.of(context).settings.arguments[0];
                      _save(preset);
                      _asyncInputDialog(context,"save");
                      //Navigator.of(context).pushReplacementNamed(Routes.mysessions);
                      },
                    child: Text("Save session", textAlign: TextAlign.center,),
                  ),
                  RaisedButton(
                    shape: StadiumBorder(),
                    color: Colors.green,
                    onPressed: () =>  {
                      _asyncInputDialog(context,"start"),
                     // Navigator.of(context).pushReplacementNamed(Routes.timer,arguments:[preset]),
                    },

                    child: Text("start", textAlign: TextAlign.center,),
                  ),
                ],
              )
            ],
          )),
    )
    );
  }
  void updateName() {
    preset.Name = "session";//descriptionController.text;
  }
  void updateDescription(String Des) {
    preset.Description = Des;
  }
  void updateDuration(int dur) {
    preset.Duration = dur;
  }
  void updateDurationStr() {
    int dur;
    if(preset!=null&&preset.Duration!=null) dur=preset.Duration*60;
    String Hs =((((dur / 60))/60)% 60).toString();
    String ms =((dur / 60) % 60).toString();
    String ss =(dur% 60).toString();
    preset.DurationStr = Hs+" : "+ms+" : "+ss;
  }

  void updateMusic(bool boola) {
    preset.Music = boola;
  }void updateMusicName(String musicName) {
    preset.MusicName = musicName;
  }void updateDate() {
    preset.Date = DateFormat.yMMMd().format(DateTime.now());
  }void updateBellRing(bool boola) {
    preset.BellRing = boola;
  }
  void updateEndIndicator() {
    preset.EndIndicator = false;
  }

  void updatebillRingInterval(int interval) {
    preset.BellRingInterval = interval;
  }



  void _save(Preset preset1) async {

    //moveToLastScreen();

    preset1.Date = DateFormat.yMMMd().format(DateTime.now());
    int result;
 // Case 2: Insert Operation
      result = await databaseHelper.insertPreset(preset1);

    if (result != 0) {  // Success
      _showAlertDialog('Status', 'Session Saved Successfully');
      //sleep(const Duration(seconds:2));


    } else {  // Failure
      _showAlertDialog('Status', 'Problem while Saving');
    //  sleep(const Duration(seconds:2));
      //Navigator.of(context).pushReplacementNamed(Routes.mysessions);
    }
    Navigator.of(context).pushReplacementNamed(Routes.mysessions);


  }
  Future<String> _asyncInputDialog(BuildContext context,String p) async {
    String teamName = '';
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Enter preset name'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Session Name', hintText: 'eg. sunSet session'),
                    onChanged: (value) {
                      teamName = value;

                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                this.preset.Name=teamName;
                Navigator.of(context).pop(teamName);
               if(p=="start") Navigator.of(context).pushReplacementNamed(Routes.timer,arguments:[preset]);
               else Navigator.of(context).pushReplacementNamed(Routes.mysessions);
              },
            ),
          ],
        );
      },
    );
  }


  void _showAlertDialog(String title, String message) {

    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),

    );
    showDialog(
        context: context,
        builder: (_) => alertDialog
    );
  }

  String _showDurationPicker(String title) {
    String choosenD="44:00";
    Duration _duration = Duration(hours: 0, minutes: 0);
    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Expanded(
              // Use it from the context of a stateful widget, passing in
              // and saving the duration as a state variable.
                child: buildPicker(context,_duration)
            ),
            new FlatButton(
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(Routes.session);
              },

              color: Colors.blue,
              //padding: EdgeInsets.fromLTRB(149,15,149,15),
              child: Text("Submit", style: TextStyle(fontSize: 20.0,color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        builder: (_) => alertDialog
    );
    return choosenD;
  }

  Widget buildPicker(BuildContext context,Duration duration1) {
    return new Scaffold(

        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Expanded(
                // Use it from the context of a stateful widget, passing in
                // and saving the duration as a state variable.
                  child: DurationPicker(
                    //duration: duration1,
                    onChange: (val) {
                      this.setState(() => duration1 = val);
                    },
                    duration: duration1,
                    snapToMins: 10.0,
                  )),
            ],
          ),
        )
    );
  }


  void moveToLastScreen(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(Routes.menu);
    //  Navigator.pop(context, true);
  }

}
