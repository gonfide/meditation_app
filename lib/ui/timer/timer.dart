import 'package:boilerplate/ui/timer/ticker.dart';
import 'package:boilerplate/utils/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';
import '../../models/meditation/preset.dart';
import '../../models/meditation/session.dart';
import '../../routes.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:boilerplate/utils/musicPlayer.dart';
import '../../sqLite/database_helper.dart';
class TimerScreen extends StatefulWidget {

  @override
  _TimerScreenState createState() => _TimerScreenState();
}

class _TimerScreenState extends State<TimerScreen> {

  musicPlayer mp=new musicPlayer();

  _TimerScreenState(){
    initState();
  }
  @override
  Widget build(BuildContext context) {

    return WillPopScope(
        onWillPop: () {
          mp.stopSound();
      moveToLastScreen(context);
    },
      child: Scaffold(
        appBar: AppBar(
          //title: const Text('My sessions'),
          leading: IconButton(

              icon:Icon(Icons.arrow_back),
              onPressed: () => {
              mp.stopSound(),
                moveToLastScreen(context),
              }
          ),
        ),
        body: BlocProvider(
          create: (context) =>  TimerBloc(ticker: Ticker()),
          child: Timer(this.mp),
        ),),);
  }
}

class Timer extends StatelessWidget {
  musicPlayer mp;
  int preparation;
  Timer(musicPlayer mpp){
    this.mp=mpp;
  }
  static const TextStyle timerTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    Preset preset=ModalRoute.of(context).settings.arguments[0];
    TimerBloc timerBloc=BlocProvider.of<TimerBloc>(context);
    timerBloc.updateDuration(preset.Preparation*60);
    timerBloc.initState();
    //timerState.duration=getDuration(context);

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen(context);
      },
    child: Scaffold(

      body: Stack(
        children: [
          Background(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 100.0),
                child: Center(
                  child: BlocBuilder<TimerBloc, TimerState>(
                    builder: (context, state) {
                      if(state.duration==null || state.duration==0)state.duration=preset.Preparation*60;
                      final String heuresStr = (((state.duration / 60)/ 60) % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      final String minutesStr = ((state.duration/60) % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      final String secondsStr = ((state.duration % 60))
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      return Text(
                        '$heuresStr:$minutesStr:$secondsStr',
                        style: Timer.timerTextStyle,
                      );
                    },
                  ),
                ),
              ),
              BlocBuilder<TimerBloc, TimerState>(
                condition: (previousState, state) =>
                state.runtimeType != previousState.runtimeType,
                builder: (context, state) => Actions(timerBloc,context,mp,preset),
              ),
            ],
          ),
        ],
      ),
    ),
    );
  }
}

class Actions extends StatelessWidget {
  musicPlayer mp;int duration;
  TimerBloc timerBloc;BuildContext context;
  Preset preset;
  //pause
  Actions( TimerBloc timerBloc,BuildContext context,musicPlayer mp,Preset pr){
    this.timerBloc=timerBloc;
    this.timerBloc.setPreset(pr);
    context=context;
    this.mp=mp;
    this.mp.setPreset(pr);
    this.preset=pr;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _mapStateToActionButtons(
        timerBloc:  this.timerBloc,context:context
      ),
    );
  }
  //TimerBloc tb=new TimerBloc(duration);
  List<Widget> _mapStateToActionButtons({
    TimerBloc timerBloc,BuildContext context
  }) {
    final TimerState currentState = timerBloc.state;
    if (currentState is Ready) {

      return [
        FloatingActionButton(
            heroTag: "btn1",
          child: Icon(Icons.play_arrow),
          onPressed: () =>{
           // playlongMusic(),
            //this.mp.playSound(currentState.duration),
            print(this.mp.getState().toString()),
            timerBloc.add(Start(currentState.duration,this.mp,false)),

          }
        ),
      ];
    }
    if (currentState is Running) {
      return [
        FloatingActionButton(
          heroTag: "btn2",
          child: Icon(Icons.pause),
          onPressed: () =>
          {
          this.mp.pauseSound(),
              timerBloc.add(Pause()),}
        ),
        FloatingActionButton(
          heroTag: "btn3",
          child: Icon(Icons.replay),
          onPressed: () =>
              {
                 this.mp.stopSound(),
                timerBloc.add(Reset()),}
        ),
      ];
    }
    if (currentState is Paused) {
      return [
        FloatingActionButton(
          heroTag: "btn4",
          child: Icon(Icons.play_arrow),
          onPressed: () => {
            this.mp.resumeSound(),
            timerBloc.add(Resume()),}
        ),
        FloatingActionButton(
          heroTag: "btn5",
          child: Icon(Icons.replay),
          onPressed: () => {

           this.mp.stopSound(),
          timerBloc.add(Reset()),}
        ),
      ];
    }
    if (currentState is Finished) {

      timerBloc.add(Start(this.preset.Duration * 60,this.mp,true));

    return [

        FloatingActionButton(
          heroTag: "btn6",
          child: Icon(Icons.replay),
          onPressed: () =>
              {
                this.mp.stopSound(),
                timerBloc.add(Reset()),}
        ),
      ];
    }
    return [];
  }
}
void moveToLastScreen(BuildContext context) {
  Preset preset=ModalRoute.of(context).settings.arguments[0];
 // _saveSession(preset);
  Navigator.of(context).pushReplacementNamed(Routes.mysessions);
  //  Navigator.pop(context, true);
}
void _saveSession(Preset preset1) async {
  DatabaseHelper databaseHelper = DatabaseHelper();
  Session ses=new Session(preset1, true);
  int result = await databaseHelper.insertSession(ses);

}
void pauseTimer(TimerBloc timerBloc,TimerState timerState,BuildContext context) {
  Navigator.of(context).pushReplacementNamed(Routes.mysessions);
  //  Navigator.pop(context, true);
}

int getDuration(BuildContext context){
  Preset preset=ModalRoute.of(context).settings.arguments[0];
  print("preset Id: "+preset.Id.toString()+" duration "+preset.Preparation.toString()+" durationStr "+preset.DurationStr.toString());
  return preset.Duration*60;
}



class Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   // Preset preset=ModalRoute.of(context).settings.arguments[0];

    return WaveWidget(
      config: CustomConfig(
        gradients: [
          [
            Color.fromRGBO(72, 74, 126, 1),
            Color.fromRGBO(125, 170, 206, 1),
            Color.fromRGBO(184, 189, 245, 0.7)
          ],
          [
            Color.fromRGBO(72, 74, 126, 1),
            Color.fromRGBO(125, 170, 206, 1),
            Color.fromRGBO(172, 182, 219, 0.7)
          ],
          [
            Color.fromRGBO(72, 73, 126, 1),
            Color.fromRGBO(125, 170, 206, 1),
            Color.fromRGBO(190, 238, 246, 0.7)
          ],
        ],
        durations: [19440, 10800, 6000],
        heightPercentages: [0.03, 0.01, 0.02],
        gradientBegin: Alignment.bottomCenter,
        gradientEnd: Alignment.topCenter,
      ),
      size: Size(double.infinity, double.infinity),
      waveAmplitude: 25,
      backgroundColor: Colors.blue[50],
    );
  }
}
