import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../routes.dart';

class SettingsPage extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<SettingsPage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  List<bool> inputs = new List<bool>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      for(int i=0;i<20;i++){
        inputs.add(true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          moveToLastScreen(context);
        },
        child: Scaffold(
            body: new ListView.builder(
                itemCount: inputs.length,
                itemBuilder: (BuildContext context, int index) {
                  return new Card(
                    child: new Container(
                      padding: new EdgeInsets.all(10.0),
                      child: new Column(
                        children: <Widget>[
                          new CheckboxListTile(
                              value: inputs[index],
                              title: new Text('item ${index}'),
                              controlAffinity: ListTileControlAffinity.leading,
                              onChanged: (bool val) {
                                ItemChange(val, index);
                              })
                        ],
                      ),
                    ),
                  );
                })));
  }


  void ItemChange(bool val,int index){
    setState(() {
      inputs[index] = val;
    });
  }


  void moveToLastScreen(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(Routes.menu);
    //  Navigator.pop(context, true);
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }
}
