import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
import '../../models/meditation/preset.dart';
import '../../routes.dart';

class DurationPickerScreen extends StatefulWidget {
  @override
  _DurationPickerScreenState createState() => _DurationPickerScreenState();
}

class _DurationPickerScreenState extends State<DurationPickerScreen> {
  Duration _duration = Duration(hours: 0, minutes: 0);

  @override
  Widget build(BuildContext context) {
    Preset preset=ModalRoute.of(context).settings.arguments[0];
    String param =ModalRoute.of(context).settings.arguments[1];
    return new Scaffold(

      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Expanded(
              // Use it from the context of a stateful widget, passing in
              // and saving the duration as a state variable.
                child: DurationPicker(
                  duration: _duration,
                  onChange: (val) {
                    this.setState(() => _duration = val);
                  },
                  snapToMins: 1.0,
                )),

            new FlatButton(
              onPressed: () {
                if(param==("preparation")) preset.Preparation=_duration.inMinutes;
                else if(param==("duration")) preset.Duration=_duration.inMinutes;
                else if(param==("interval")) preset.BellRingInterval=_duration.inMinutes;
                Navigator.of(context).pushReplacementNamed(Routes.session,arguments:[preset,param,false]);

              },

                  color: Colors.blue,
                  padding: EdgeInsets.fromLTRB(149,15,149,15),
              child: Text("Submit", style: TextStyle(fontSize: 20.0,color: Colors.white),
              ),
                )
          ],
        ),
      )
    );
  }
}
