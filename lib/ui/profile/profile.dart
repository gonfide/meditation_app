import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../routes.dart';
class ProfilePage extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
      moveToLastScreen(context);
    },
    child: Scaffold(
        body: new Container(
          color: Colors.white,
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  new Container(
                 //   height: 250.0,
                    color: Colors.white,
                    child: new Column(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 5.0),
                            child: new Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new IconButton(

                                  icon: new Icon(Icons.arrow_back),
                                  onPressed: () => Navigator.of(context).pushReplacementNamed(Routes.menu),
                                ),

                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(top: 0.0),
                          child: new Stack(fit: StackFit.loose, children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                    width: 140.0,
                                    height: 140.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        image:
                                        new ExactAssetImage('assets/images/profile.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                    )),
                              ],
                            ),
                          ]),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    color: Color(0xffFFFFFF),
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 25.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                   new Row(
                   // mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,

                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Name : ',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'jhon jack',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new IconButton(

                                        icon: new Icon(Icons.edit),
                                        onPressed: () => _asyncInputDialog(context),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                          Padding(
                              padding: EdgeInsets.only( left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,

                      //          mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Container(
                                    //width: 50.0,
                                    //height: 50.0,
                                    padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.rectangle,// You can use like this way or like the below line
                                      //borderRadius: new BorderRadius.circular(30.0),
                                      color: Colors.orangeAccent,
                                    ),
                                    child: new Text("21H 3MIN 55SEC \n Total meditation time ", style: new TextStyle(color: Colors.white, fontSize: 15.0),textAlign: TextAlign.center,),// You can add a Icon instead of text also, like below.
                                    //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                  ),


                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only( left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,

                                //          mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new InkWell(// this is the one you are looking for..........
                                    onTap: () =>  {

                                      Navigator.of(context).pushReplacementNamed(Routes.history),
                                    },
                                    child: new Container(
                                      //width: 50.0,
                                      //height: 50.0,
                                      //      margin: new EdgeInsets.symmetric(horizontal: 40.0),
                                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,// You can use like this way or like the below line
                                        //borderRadius: new BorderRadius.circular(30.0),
                                        color: Colors.green,
                                      ),
                                      child: new Text("4 SESSIONS", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                    ),),

                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only( left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,

                                //          mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new InkWell(// this is the one you are looking for..........
                                    onTap: () =>  {
                                     // Navigator.of(context).pushReplacementNamed(Routes.history),
                                    },
                                    child: new Container(
                                      //width: 50.0,
                                      //height: 50.0,
                                      //      margin: new EdgeInsets.symmetric(horizontal: 40.0),
                                      padding: const EdgeInsets.all(30.0),//I used some padding without fixed width and height
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,// You can use like this way or like the below line
                                        //borderRadius: new BorderRadius.circular(30.0),
                                        color: Colors.blue,
                                      ),
                                      child: new Text("Meditation Diagram", style: new TextStyle(color: Colors.white, fontSize: 15.0)),// You can add a Icon instead of text also, like below.
                                      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                    ),),

                                ],
                              ))

                         // !_status ? _getActionButtons() : new Container(),
                        ],
                      ),
                    ),
                  )

                ],
              ),
            ],
          ),
        )));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                    child: new Text("Save"),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () {
                      setState(() {
                        _status = true;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                    child: new Text("Cancel"),
                    textColor: Colors.white,
                    color: Colors.red,
                    onPressed: () {
                      setState(() {
                        _status = true;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Future<String> _asyncInputDialog(BuildContext context) async {
    String teamName = '';
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Enter Your name'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Name', hintText: 'eg. Jhon'),
                    onChanged: (value) {
                      teamName = value;
                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop(teamName);
              },
            ),
          ],
        );
      },
    );
  }

  void moveToLastScreen(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(Routes.menu);
    //  Navigator.pop(context, true);
  }
  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }
}