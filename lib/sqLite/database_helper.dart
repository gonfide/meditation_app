import 'package:boilerplate/models/meditation/session.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import '../models/meditation/preset.dart';
import '../models/User.dart';
class DatabaseHelper {

  static DatabaseHelper _databaseHelper;    // Singleton DatabaseHelper
  static Database _database;                // Singleton Database
  String presetTable = 'preset_table';
  String sessionTable = 'session_table';
  String userTable = 'user_table';
  String colId = 'Id';
  String colName = 'Name';
  String colDescription = 'Description';
  String colPreparation = 'Preparation';
  String colDuration= 'Duration';
  String colBellRing= 'BellRing' ;
  String colEndIndicator = 'EndIndicator';
  String colBellRingInterval= 'BellRingInterval';
  String colMusic = 'Music';
  String colMusicName = 'MusicName';
  String colBillSound = 'BillSound';
  String colEndMusic = 'EndMusic';
  String colDate= 'Date';

  String colHours= 'Hours';
  String colSessions= 'Sessions';
  DatabaseHelper._createInstance(); // Named constructor to create instance of DatabaseHelper

  factory DatabaseHelper() {

    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper._createInstance(); // This is executed only once, singleton object
    }
    return _databaseHelper;
  }

  Future<Database> get database async {

    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    // Get the directory path for both Android and iOS to store database.
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'sessions.db';

    // Open/create the database at a given path
    var presetsDatabase = await openDatabase(path, version: 1, onCreate: _createDb);
    return presetsDatabase;
  }

  void _createDb(Database db, int newVersion) async {

    await db.execute('CREATE TABLE $presetTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colDescription TEXT, $colPreparation INTEGER, $colDuration INTEGER, $colBellRing BOOLEAN,$colBellRingInterval INTEGER, $colEndIndicator BOOLEAN, $colMusic BOOLEAN, $colMusicName TEXT, $colBillSound TEXT, $colEndMusic TEXT, $colDate TEXT)');
    await db.execute('CREATE TABLE $sessionTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colDescription TEXT, $colPreparation INTEGER, $colDuration INTEGER, $colBellRing BOOLEAN,$colBellRingInterval INTEGER, $colEndIndicator BOOLEAN, $colMusic BOOLEAN, $colMusicName TEXT, $colBillSound TEXT, $colEndMusic TEXT, $colDate TEXT)');
    await db.execute('CREATE TABLE $userTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colHours INTEGER, $colSessions INTEGER)');

  }

  // Fetch Operation: Get all note objects from database
  Future<List<Map<String, dynamic>>> getPresetMapList() async {
    Database db = await this.database;

//		var result = await db.rawQuery('SELECT * FROM $noteTable order by $colPriority ASC');
    var result = await db.query(presetTable,orderBy: '$colId DESC');
    return result;
  }

  // Insert Operation: Insert a Note object to database
  Future<int> insertPreset(Preset preset) async {
    Database db = await this.database;
    var result = await db.insert(presetTable, preset.toMap());
    return result;
  }

  Future<int> insertSession(Session session) async {
    Database db = await this.database;
    var result = await db.insert(sessionTable, session.toMap());
    return result;
  }

  Future<int> insertUser(User user) async {
    Database db = await this.database;
    var result = await db.insert(userTable, user.toMap());
    return result;
  }
  Future<int> updateUser(User user) async {
    var db = await this.database;
    var result = await db.update(userTable, user.toMap(), where: '$colId = ?', whereArgs: [user.id]);
    return result;
  }

  // Update Operation: Update a Note object and save it to database
  Future<int> updatePreset(Preset preset) async {
    var db = await this.database;
    var result = await db.update(presetTable, preset.toMap(), where: '$colId = ?', whereArgs: [preset.Id]);
    return result;
  }

  Future<int> updateSession(Session session) async {
    var db = await this.database;
    var result = await db.update(sessionTable, session.toMap(), where: '$colId = ?', whereArgs: [session.Id]);
    return result;
  }

  Future<Session> getSession(int id) async {
    Database db = await this.database;
    var result = await db.rawQuery('SELECT * FROM $sessionTable WHERE $colId = $id');
    print("hahahahahahahahaha");
    print(result);
    Session session=null;
    if(result.length!=0 && result[0]!=null) {session=Session.fromMapObject(result[0]); return session;}
    return null;
  }

  // Delete Operation: Delete a Note object from database
  Future<int> deletePreset(int id) async {
    var db = await this.database;
    int result = await db.rawDelete('DELETE FROM $presetTable WHERE $colId = $id');
    return result;
  }

  // Get number of Note objects in database
  Future<int> getCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $presetTable');
    int result = Sqflite.firstIntValue(x);
    return result;
  }

  // Get the 'Map List' [ List<Map> ] and convert it to 'Note List' [ List<Note> ]
  Future<List<Preset>> getPresetList() async {

    var presetMapList = await getPresetMapList(); // Get 'Map List' from database
    int count = presetMapList.length;         // Count the number of map entries in db table

    List<Preset> presetList = List<Preset>();
    // For loop to create a 'Note List' from a 'Map List'
    for (int i = 0; i < count; i++) {
      presetList.add(Preset.fromMapObject(presetMapList[i]));
    }

    return presetList;
  }

  Future<List<Map<String, dynamic>>> getSessionMapList() async {
    Database db = await this.database;

//		var result = await db.rawQuery('SELECT * FROM $noteTable order by $colPriority ASC');
    var result = await db.query(sessionTable,orderBy: '$colId DESC');
    return result;
  }

  Future<List<Session>> getSessionList() async {

    var presetMapList = await getSessionMapList(); // Get 'Map List' from database
    int count = presetMapList.length;         // Count the number of map entries in db table

    List<Session> presetList = List<Session>();
    // For loop to create a 'Note List' from a 'Map List'
    for (int i = 0; i < count; i++) {
      presetList.add(Session.fromMapObject(presetMapList[i]));
    }

    return presetList;
  }




}






