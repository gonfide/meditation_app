import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import '../sqLite/database_helper.dart';
import '../models/meditation/session.dart';
import '../models/meditation/preset.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:path_provider/path_provider.dart';
import 'package:audioplayers/audio_cache.dart';

class musicPlayer {

  static AudioCache _player = AudioCache();
  static Preset preset;
  setPreset(Preset preset){
    musicPlayer.preset=preset;
  }
  static final AudioPlayer audioPlugin = AudioPlayer();
  int musicduration;
  static String mp3Uri="";
  Future<String> load() async {
    final ByteData data = await rootBundle.load('assets/music/alarm.mp3');
    Directory tempDir = await getTemporaryDirectory();
    File tempFile = File('${tempDir.path}/alarm.mp3');
    await tempFile.writeAsBytes(data.buffer.asUint8List(), flush: true);
    mp3Uri = tempFile.uri.toString();
    print('finished loading, uri=$mp3Uri');
    return mp3Uri;
  }


  void playonly(int duration)
  {
    final DateTime now = DateTime.now();
    if(duration==0) audioPlugin.stop();
    else {
      audioPlugin.play(mp3Uri, isLocal: true);
      DateTime after;
      audioPlugin.onPlayerCompletion.listen((p) =>
      {
        after = DateTime.now(),
        if(((after.difference(now).inSeconds) < duration)){
          playonly(duration - (after.difference(now).inSeconds.toInt()))

        }
        else  audioPlugin.stop(),
      });
    }
  }

AudioPlayerState getState() {
    return audioPlugin.getMyState();
}

  void stopSound() {
    if (mp3Uri != null) {
      audioPlugin.stop();
    }
  }
  void pauseSound() {
    if (mp3Uri != null) {
      audioPlugin.pause();
    }
  }

  void resumeSound() {
    if (mp3Uri != null) {
      audioPlugin.play(mp3Uri, isLocal: true);
    }
  }

  void saveSession() async {
    DatabaseHelper databaseHelper = DatabaseHelper();

    Future<Session> ss=databaseHelper.getSession(musicPlayer.preset.Id);
    ss.then((session) async {
      if(session!=null && session.Id!=null){
        int result = await databaseHelper.updateSession(session);
      }
      else {
        Session ses=new Session(musicPlayer.preset, true);
        int result = await databaseHelper.insertSession(ses);
      }

    });





  }
}