import 'package:boilerplate/utils/musicPlayer.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:boilerplate/utils/musicPlayer.dart';
abstract class TimerEvent extends Equatable {
   TimerEvent();

  @override
  List<Object> get props => [];
}

class Start extends TimerEvent {
  int duration;
  bool isMusic;
  musicPlayer mp=new musicPlayer();


  Start(@required int duration, musicPlayer mp,bool isMusic)
  {  this.duration=duration;
     this.mp=mp;
     this.isMusic=isMusic;
     //this.mp.playSound(this.duration);

  }

  @override
  String toString() => "Start { duration: $duration }";
}

class Pause extends TimerEvent {}

class Resume extends TimerEvent {}

class Reset extends TimerEvent {}

class Tick extends TimerEvent {
   int duration;
   musicPlayer mp;
   bool isPlay;

  Tick(@required int duration,musicPlayer mp,bool isMusic)
        {
          this.duration=duration;
          this.mp=mp;
          if(isMusic) this.mp.playonly(this.duration);
          this.mp.saveSession();
        }

  @override
  List<Object> get props => [duration];

  @override
  String toString() => "Tick { duration: $duration }";
}