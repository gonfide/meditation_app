import 'dart:async';
import 'package:boilerplate/ui/timer/ticker.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'bloc.dart';
import '../../models/meditation/preset.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:boilerplate/utils/musicPlayer.dart';
class TimerBloc extends Bloc<TimerEvent, TimerState> {
  musicPlayer mp=new musicPlayer();
  Preset preset;

  final Ticker _ticker;
  static int duration=0;
 updateDuration(int duree){
   duration=duree;
 }
  updatePreset(int duree){
    print(this.preset.Id);
    mp.setPreset(this.preset);

  }
  void initState() {
    mp.load();


  }
  StreamSubscription<int> _tickerSubscription;


  TimerBloc({@required Ticker ticker})
      : assert(ticker != null),
        _ticker = ticker;
   void setPreset(Preset preset){
    this.preset=preset;
  }

  @override
  TimerState get initialState => Ready(duration);

  @override
  void onTransition(Transition<TimerEvent, TimerState> transition) {
    super.onTransition(transition);
    print(transition);
  }

  @override
  Stream<TimerState> mapEventToState(
      TimerEvent event,
      ) async* {
    if (event is Start) {
      yield* _mapStartToState(event,event.isMusic);
    } else if (event is Pause) {
      yield* _mapPauseToState(event);
    } else if (event is Resume) {
      yield* _mapResumeToState(event);
    } else if (event is Reset) {
      yield* _mapResetToState(event);
    } else if (event is Tick) {
      yield* _mapTickToState(event);
    }
  }

  @override
  Future<void> close() {
    _tickerSubscription?.cancel();
    return super.close();
  }

  Stream<TimerState> _mapStartToState(Start start,bool isMusic) async* {
    yield Running(start.duration);
    _tickerSubscription?.cancel();
    _tickerSubscription = _ticker
        .tick(ticks: start.duration)
        .listen((duration) => add(Tick(duration,this.mp,isMusic)));
  }

  Stream<TimerState> _mapPauseToState(Pause pause) async* {
    if (state is Running) {
      _tickerSubscription?.pause();
      yield Paused(state.duration);
    }
  }

  Stream<TimerState> _mapResumeToState(Resume pause) async* {
    if (state is Paused) {
      _tickerSubscription?.resume();
      yield Running(state.duration);
    }
  }

  Stream<TimerState> _mapResetToState(Reset reset) async* {
    _tickerSubscription?.cancel();
    yield Ready(duration);
  }

  Stream<TimerState> _mapTickToState(Tick tick) async* {
    yield tick.duration > 0 ? Running(tick.duration) : Finished();
  }
}