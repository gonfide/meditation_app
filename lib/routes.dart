import 'package:boilerplate/ui/home/menu.dart';
import 'package:boilerplate/ui/home/mysessions.dart';
import 'package:boilerplate/ui/home/session.dart';
import 'package:boilerplate/ui/home/mypresets.dart';
import 'package:boilerplate/ui/home/BillSounds.dart';
import 'package:boilerplate/ui/home/musicSounds.dart';
import 'package:boilerplate/ui/profile/profile.dart';
import 'package:boilerplate/ui/settings/durationpicket.dart';
import 'package:boilerplate/ui/settings/settings.dart';
import 'package:boilerplate/ui/timer/timer.dart';
import 'package:flutter/material.dart';

import 'ui/home/home.dart';
import 'ui/login/login.dart';
import 'ui/splash/splash.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String login = '/login';
  static const String home = '/home';
  static const String menu = '/menu';
  static const String session = '/session';
  static const String mysessions = '/mysessions';
  static const String durationpicker = '/durationpicker';
  static const String timer = '/timer';
  static const String billSounds = '/billSounds';
  static const String musicSounds = '/musicSounds';
  static const String profile = '/profile';
  static const String history = '/history';
  static const String settings = '/settings';
  static final routes = <String, WidgetBuilder>{

    splash: (BuildContext context) => SplashScreen(),
    login: (BuildContext context) => LoginScreen(),
    home: (BuildContext context) => HomeScreen(),
    menu: (BuildContext context) => MenuScreen(),
    session: (BuildContext context) => SessionScreen(),
    mysessions: (BuildContext context) => MyPresetsScreen(),
    durationpicker: (BuildContext context) => DurationPickerScreen(),
    timer: (BuildContext context) => TimerScreen(),
    billSounds: (BuildContext context) => BillSoundScreen(),
    musicSounds: (BuildContext context) => MusicSoundsScreen(),
    profile: (BuildContext context) => ProfilePage(),
    history: (BuildContext context) => MySessionsScreen(),
    settings: (BuildContext context) => SettingsPage(),


  };
}



